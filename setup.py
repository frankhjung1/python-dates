import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="python-age",
    version="0.1.0",
    author="Frank Jung",
    author_email="frank.jung@marlo.com.au",
    description="Show your age.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/frankhjung/python-age",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: GPL 3",
        "Operating System :: OS Independent",
    ],
)
