#!/usr/bin/env make

.DEFAULT_GOAL	:= default
CODE_COVERAGE	:= 90	# minimum percentage for code coverage
CTAGS		:= $(shell which ctags)
LINE_LENGTH	:= 79	# PEP-8 standards ensure styling tools use this too
PIP		:= $(shell which pip3)
PYTHON		:= $(shell which python3)
RM		:= rm -rf
SRCS		:= $(wildcard main.py tests/*.py)

default: format check test

.PHONY: all
all:	format check test run doc

.PHONY: help
help:
	@echo
	@echo "Default goal: ${.DEFAULT_GOAL}"
	@echo
	@echo "  all:    style and test"
	@echo "  preen:  format and lint"
	@echo "  format: format code, sort imports and requirements"
	@echo "  lint:   check code"
	@echo "  test:   run unit tests"
	@echo "  doc:    document module"
	@echo "  clean:  delete all generated files"
	@echo
	@echo "Initialise virtual environment (.venv) with:"
	@echo
	@echo "  pip3 install -U virtualenv; python3 -m virtualenv .venv; source .venv/bin/activate; pip3 install -Ur requirements.txt"
	@echo
	@echo "Start virtual environment (.venv) with:"
	@echo
	@echo "  source .venv/bin/activate"
	@echo
	@echo Deactivate with:
	@echo
	@echo "  deactivate"
	@echo

.PHONY: format
format:
	# sort imports
	@isort --verbose --line-length $(LINE_LENGTH) --profile black $(SRCS)
	# format code to googles style
	@black --verbose --line-length $(LINE_LENGTH) $(SRCS)
	# sort requirements
	@sort-requirements requirements.txt

.PHONY: check
check:	tags lint

.PHONY: tags
tags:
ifdef CTAGS
	# build ctags for vim
	@ctags --recurse -o tags $(SRCS)
endif

.PHONY: lint
lint:
	# check with flake8
	@flake8 --verbose $(SRCS)
	# lint with pylint
	@pylint --verbose $(SRCS)
	# security checks with bandit
	@bandit --verbose --configfile .bandit.yaml --recursive $(SRCS)

.PHONY: test
test:
	pytest -v tests/test*.py

.PHONY: run
run:
	$(PYTHON) -m main

.PHONY: clean
clean:
	# clean generated artefacts
	-$(RM) -rf __pycache__ **/__pycache__
	-$(RM) -rf .coverage
	-$(RM) -rf .pytest_cache
	-$(RM) -rf cover
	-$(RM) -rf public
	-$(RM) -rf target
	-$(RM) -v *.pyc *.pyo *.py,cover
	-$(RM) -v **/*.pyc **/*.pyo **/*.py,cover