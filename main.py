#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# pylint: disable=C0103
""" Show age in years months and days. """

from datetime import datetime

from dateutil.relativedelta import SU, relativedelta

if __name__ == "__main__":
    # Example 1 - How old was Richard Feynman when he died?
    birth = datetime(1918, 5, 11)
    death = datetime(1988, 2, 15)

    age = relativedelta(death, birth)
    print(
        f"Richard Feynman was {age.years} years, {age.months} months,",
        f"{age.days} days old when he died.",
    )

    # Example 2 - Date of the Sunday a week before today?
    today = datetime.now()
    delta = relativedelta(today, weekday=SU(-1))
    sunday = today - delta
    print(f"The previous Sunday is {sunday.strftime('%d %B %Y')}.")

    # Example 3 - How many days until the next birthday?
    birthday = datetime(2020, 5, 11)
    delta = relativedelta(birthday, today)
    print(f"There are {delta.days} days until my birthday.")

    # Example 4 How many Sundays between two dates?
    start = datetime(2020, 5, 11)
    end = datetime(2020, 5, 18)
    delta = relativedelta(end, start, weekday=SU())
    print(f"There are {delta.days} Sundays between {start} and {end}.")
