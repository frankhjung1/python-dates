# Python How to Work with Dates

## Documentation & Reports

* [Bandit Security Report](https://frankhjung1.gitlab.io/python-dates/bandit_report.html)
* [Flake8 Violations](https://frankhjung1.gitlab.io/python-dates/flake8/index.html)
* [Lint Report](https://frankhjung1.gitlab.io/python-dates/pylint_report.html)
* [Module Documentation](https://frankhjung1.gitlab.io/python-dates/index.html)
* [Unit Test Report](https://frankhjung1.gitlab.io/python-dates/pytest_report.html)

## Examples

### Example 1 - How old was Richard Feynman when he died?

Expect: `Richard Feynman was 69 years, 9 months, 4 days old when he died.`

### Example 2 - Date of the Sunday a week before today?

Given:

```text
$ date
Fri 25 Sep 2020 13:53:45 AEST
$ ncal
    September 2020
Su     6 13 20 27
Mo     7 14 21 28
Tu  1  8 15 22 29
We  2  9 16 23 30
Th  3 10 17 24
Fr  4 11 18 25
Sa  5 12 19 26
```

Expect: `The previous Sunday is 20 September 2020.`

## Jupyter Notebook

See [example](example.ipynb).
