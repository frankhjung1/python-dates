#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test date functions.
"""

import unittest
from datetime import datetime

from dateutil.relativedelta import relativedelta


class TestDates(unittest.TestCase):
    """Test date functions."""

    def setUp(self):
        self.bday = datetime(1964, 11, 29)

    def test_1_day(self):
        """Difference of 1 day."""
        todate = datetime(1964, 11, 30)
        age = relativedelta(todate, self.bday)
        self.assertEqual(0, age.years)
        self.assertEqual(0, age.months)
        self.assertEqual(1, age.days)

    def test_1_month(self):
        """Difference of 1 month."""
        todate = datetime(1964, 12, 29)
        age = relativedelta(todate, self.bday)
        self.assertEqual(0, age.years)
        self.assertEqual(1, age.months)
        self.assertEqual(0, age.days)

    def test_1_year(self):
        """Difference of 1 year."""
        todate = datetime(1965, 11, 29)
        age = relativedelta(todate, self.bday)
        self.assertEqual(1, age.years)
        self.assertEqual(0, age.months)
        self.assertEqual(0, age.days)
